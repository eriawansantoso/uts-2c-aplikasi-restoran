package santoso.eriawan.aplikasimenusederhana

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_minuman.view.*


class FragmentMinuman : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btInsertMnm -> {
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btUpdateMnm-> {
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btDeleteMnm -> {
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
        }
    }
    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v :View
    lateinit var builder : AlertDialog.Builder
    var idMinum: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()
        v = inflater.inflate(R.layout.frag_minuman,container,false)
        v.btUpdateMnm.setOnClickListener(this)
        v.btInsertMnm.setOnClickListener(this)
        v.btDeleteMnm.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsMinuman.setOnItemClickListener(itemClick)

        return v
    }
    fun showDataMinuman(){
        val cursor : Cursor = db.query("minum", arrayOf("nm_minum","deskripsi","harga", "id_minum as _id"),
            null, null, null,null,"_id asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_minuman,cursor,
            arrayOf("_id","nm_minum","deskripsi","harga"), intArrayOf(R.id.txidMnm, R.id.txNmMnm,R.id.txDescMnm, R.id.txHargaMnm),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMinuman.adapter = adapter
    }
    override fun onStart() {
        super.onStart()
        showDataMinuman()
    }

    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idMinum = c.getString(c.getColumnIndex("_id"))
        v.namaMnm.setText(c.getString(c.getColumnIndex("nm_minum")))
        v.deskripsiMnm.setText(c.getString(c.getColumnIndex("deskripsi")))
        v.hargaMnm.setText(c.getString(c.getColumnIndex("harga")))
    }
    fun insertDataMinum(nama : String, deskripsi : String, harga : String){
        var cv : ContentValues = ContentValues()
        cv.put("nm_minum",nama)
        cv.put("deskripsi",deskripsi)
        cv.put("harga",harga)
        db.insert("minum", null,cv)
        showDataMinuman()
    }
    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMinum(v.namaMnm.text.toString(), v.deskripsiMnm.text.toString(), v.hargaMnm.text.toString())
        v.namaMnm.setText("")
        v.deskripsiMnm.setText("")
        v.hargaMnm.setText("")
    }

    fun updateDataMinum(nama: String, deskripsi : String, harga : String, idMinum: String){
        var cv : ContentValues = ContentValues()
        cv.put("nm_minum",nama)
        cv.put("deskripsi",deskripsi)
        cv.put("harga",harga)
        db.update("minum",cv,"id_minum = $idMinum",null)
        showDataMinuman()
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataMinum(v.namaMnm.text.toString(), v.deskripsiMnm.text.toString(), v.hargaMnm.text.toString(), idMinum)
        v.namaMnm.setText("")
        v.deskripsiMnm.setText("")
        v.hargaMnm.setText("")
    }
    fun deleteDataMinum(idMinum : String){
        db.delete("minum","id_minum = $idMinum", null)
        showDataMinuman()
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMinum(idMinum)
        v.namaMnm.setText("")
        v.deskripsiMnm.setText("")
        v.hargaMnm.setText("")
    }
}