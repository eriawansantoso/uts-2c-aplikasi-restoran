package santoso.eriawan.aplikasimenusederhana


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.sosmed.view.*


class FragmentSosmed : Fragment(), View.OnClickListener  {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.admin1-> {
                var webUri = "mailto:rizkadewi@gmail.com"
                var intentInternet = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(webUri)
                ) //filter application than can open website
                startActivity(intentInternet)
            }
            R.id.admin2 -> {
                var webUri = "mailto:eriawansantoso45@gmail.com"
                var intentInternet = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(webUri)
                ) //filter application than can open website
                startActivity(intentInternet)
            }
        }
    }
    lateinit var thisParent: MainActivity
    lateinit var v :View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.sosmed,container,false)
        v.admin1.setOnClickListener(this)
        v.admin2.setOnClickListener(this)


        return v
    }

}
