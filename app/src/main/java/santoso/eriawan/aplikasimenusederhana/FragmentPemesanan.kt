package santoso.eriawan.aplikasimenusederhana

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_pemesanan.*
import kotlinx.android.synthetic.main.frag_pemesanan.view.*
import kotlinx.android.synthetic.main.frag_pemesanan.view.spMkn

class FragmentPemesanan :  Fragment(), View.OnClickListener {
    lateinit var thisParent: MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var adapterMkn: SimpleCursorAdapter
    lateinit var adapterMnm: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    lateinit var db: SQLiteDatabase
    var arrMkn = ArrayList<String>()
    var arrMnm = ArrayList<String>()
    var namaMkn: String = ""
    var namaMnm: String = ""
    var id_pesan: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_pemesanan, container, false)
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        v.spMkn.onItemSelectedListener = MknSelect
        v.spMnm.onItemSelectedListener = MnmSelect
        v.btDeletePesan.setOnClickListener(this)
        v.btInsertPesan.setOnClickListener(this)
        v.btUpdatePesan.setOnClickListener(this)
//        v.rg1.setOnCheckedChangeListener = rgselect
        v.lsPesan.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMkn()
        showDataMnm()
        showDataPesan()
    }

    //    makan
    val MknSelect = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spMkn.setSelection(0, true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c: Cursor = adapterMkn.getItem(position) as Cursor
            namaMkn = c.getString(c.getColumnIndex("_id"))
        }
    }

    //    minum
    val MnmSelect = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spMnm.setSelection(0, true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c: Cursor = adapterMnm.getItem(position) as Cursor
            namaMnm = c.getString(c.getColumnIndex("_id"))
        }
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        id_pesan = c.getInt(c.getColumnIndex("_id"))
        namaMkn = c.getString(c.getColumnIndex("namaMkn"))
        namaMnm = c.getString(c.getColumnIndex("namaMnm"))
        v.jmlMkn.setText(c.getString(c.getColumnIndex("qtyMkn")))
        v.jmlMnm.setText(c.getString(c.getColumnIndex("qtyMnm")))
        v.kondisi.setText(c.getString(c.getColumnIndex("kondisi")))
        v.spMkn.setSelection(getIndex(v.spMkn, namaMkn, arrMkn))
        v.spMnm.setSelection(getIndex(v.spMnm, namaMnm, arrMkn))
    }

    fun getIndex(spinner: Spinner, myString: String, arrayL: ArrayList<String>): Int {
        var a = spinner.count
        Log.d("Output", "Jumlah item dalam array = $a")
        var b: String = ""
        for (i in 0 until a) {
            b = arrayL.get(i)
            Log.d("Output", "Index ke $i = $b")
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }
    fun showDataPesan(){
        var sql = "select n.id_pesan as _id, m.nm_makan as namaMkn, n.qtyMkn ,k.nm_minum as namaMnm, n.qtyMnm, n.kondisi from pesan n, makan m , minum k " +
                "where n.id_makan=m.id_makan and n.id_minum=k.id_minum "
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_pemesanan,c,
            arrayOf("_id","namaMkn","qtyMkn","namaMnm","qtyMnm","kondisi"), intArrayOf(R.id.txidPesan,R.id.txMakanan,R.id.txQtyMkn,R.id.txMinuman,
                R.id.txQtyMnm,R.id.txKet),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsPesan.adapter = lsAdapter
    }
    fun showDataMkn(){
        val c : Cursor = db.rawQuery("select nm_makan as _id from makan order by nm_makan asc",null)
        adapterMkn = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        adapterMkn.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMkn.adapter = adapterMkn
        v.spMkn.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrMkn.add(temp) //add the item
            c.moveToNext()
        }
    }
    fun showDataMnm(){
        val c : Cursor = db.rawQuery("select nm_minum as _id from minum order by nm_minum asc",null)
        adapterMnm = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        adapterMnm.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMnm.adapter = adapterMnm
        v.spMnm.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrMnm.add(temp) //add the item
            c.moveToNext()
        }
    }
    fun insertDataPesan(id_makan : Int, qtyMkn: String, id_minum : Int, qtyMnm : String, kondisi : String){
        var sql = "insert into pesan(id_makan, qtyMkn, id_minum,qtyMnm,kondisi) values (?,?,?,?,?)"
        db.execSQL(sql, arrayOf(id_makan, qtyMkn, id_minum,qtyMnm,kondisi))
        showDataPesan()
    }
    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sqlMkn = "select id_makan from makan where nm_makan = '$namaMkn'"
        var sqlMnm = "select id_minum from minum where nm_minum = '$namaMnm'"
        val c : Cursor = db.rawQuery(sqlMkn,null)
        val c1 : Cursor = db.rawQuery(sqlMnm,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataPesan(
                c.getInt(c.getColumnIndex("id_makan")),
                v.jmlMkn.text.toString(),
                c1.getInt(c1.getColumnIndex("id_minum")),
                v.jmlMnm.text.toString(),
                v.kondisi.text.toString()
            )
            v.spMkn.setSelection(0)
            v.jmlMkn.setText("")
            v.spMnm.setSelection(0)
            v.jmlMnm.setText("")
            v.kondisi.setText("")
        }
    }
    fun deleteDataPesan(id_pesan: Int){
        db.delete("pesan","id_pesan = $id_pesan",null)
        showDataPesan()
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataPesan(id_pesan)
        v.spMkn.setSelection(0)
        v.jmlMkn.setText("")
        v.spMnm.setSelection(0)
        v.jmlMnm.setText("")
        v.kondisi.setText("")
    }
    fun updateDataPesan(id_makan : Int, qtyMkn: String, id_minum : Int, qtyMnm : String, kondisi : String,id_pesan : Int){
        var cv : ContentValues = ContentValues()
        cv.put("id_makan",id_makan)
        cv.put("qtyMkn",qtyMkn)
        cv.put("id_minum",id_minum)
        cv.put("qtyMnm",qtyMnm)
        cv.put("kondisi",kondisi)
        db.update("pesan",cv,"id_pesan = $id_pesan",null)
        showDataPesan()
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sqlMkn = "select id_makan from makan where nm_makan = '$namaMkn'"
        var sqlMnm = "select id_minum from minum where nm_minum = '$namaMnm'"
        val c : Cursor = db.rawQuery(sqlMkn,null)
        val c1 : Cursor = db.rawQuery(sqlMnm,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            updateDataPesan(
                c.getInt(c.getColumnIndex("id_makan")),
                v.jmlMkn.text.toString(),
                c1.getInt(c1.getColumnIndex("id_minum")),
                v.jmlMnm.text.toString(),
                v.kondisi.text.toString(),
                id_pesan
            )
            v.spMkn.setSelection(0)
            v.jmlMkn.setText("")
            v.spMnm.setSelection(0)
            v.jmlMnm.setText("")
            v.kondisi.setText("")
        }
    }
    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btInsertPesan -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btDeletePesan -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Yakin akan menghapus data ini?")
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btUpdatePesan -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }
}