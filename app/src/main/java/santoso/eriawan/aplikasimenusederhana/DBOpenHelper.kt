package santoso.eriawan.aplikasimenusederhana

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper (context: Context): SQLiteOpenHelper(context,DB_Name, null, DB_Ver) {
    companion object {
        val DB_Name = "menu"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tMkn = "create table makan(id_makan integer primary key autoincrement,nm_makan text not null, deskripsi text not null, harga varchar not null)"
        val tMnm = "create table minum(id_minum integer primary key autoincrement,nm_minum text not null, deskripsi text not null, harga varchar not null)"
        val tPesan ="create table pesan(id_pesan integer primary key autoincrement, id_makan int not null, qtyMkn varchar not null, id_minum int not null, qtyMnm varchar not null, kondisi text not null)"
        db?.execSQL(tMkn)
        db?.execSQL(tPesan)
        db?.execSQL(tMnm)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }
}