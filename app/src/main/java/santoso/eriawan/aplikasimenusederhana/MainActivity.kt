package santoso.eriawan.aplikasimenusederhana

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    lateinit var db : SQLiteDatabase
    lateinit var fragMakan : FragmentMakanan
    lateinit var fragMinum : FragmentMinuman
    lateinit var fragPesan : FragmentPemesanan
    lateinit var fragSos : FragmentSosmed
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragMakan = FragmentMakanan()
        fragMinum = FragmentMinuman()
        fragPesan = FragmentPemesanan()
        fragSos = FragmentSosmed()
        db = DBOpenHelper(this).writableDatabase
    }
    fun getDBObject() : SQLiteDatabase {
        return db
    }
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.itemMakanan -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragMakan).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMinuman -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragMinum).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemPesanan -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragPesan).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemSosmed -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragSos).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemHome-> frameLayout.visibility = View.GONE
        }
        return true
    }
    }


